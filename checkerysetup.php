<?php
wp_register_style( 'checkerybootstrap', plugins_url('css/bootstrap.min.css', __FILE__) ); ?>
<link href="<?php echo plugins_url("css/foo/footable.core.css?v=2-0-1", __FILE__);?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo plugins_url("css/foo/footable.standalone.css", __FILE__);?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo plugins_url("css/foo/footable-demos.css", __FILE__);?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo plugins_url("js/foo/footable.js?v=2-0-1", __FILE__);?>" type="text/javascript"></script>
<script src="<?php echo plugins_url("js/foo/footable.sort.js?v=2-0-1", __FILE__);?>" type="text/javascript"></script>
<?php wp_enqueue_style('checkerybootstrap');	

if(isset($_GET['flag']))
{
	if(trim($_GET['flag']) == 'edit')
	{
	
	$results = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."checkery_checklist WHERE id='".$_GET['id']."' ORDER BY id ASC");
//echo '<pre>';print_r($results);exit;
	}
}

?>
<style>
.wp-media-buttons .insert-media, .wp-editor-tabs
{
	display:none !important;
}
#poststuff,#title
{
	
}.field.switch > span {
    display: block;
    float: left;
    line-height: 30px;
    padding: 0 20px;
	font-weight:bold;
}
#description_ifr
{
	
    height: 176px !important;
    width: 100% !important;
}

</style>
<link href="<?php echo plugins_url("css/checkery.css", __FILE__);?>" rel="stylesheet" type="text/css"/>
<h2><?php echo ($results[0]->checklist != '') ? 'Update '.stripslashes($results[0]->checklist):'Add New Checklist';?></h2>
<hr />
<?php
if( $_POST['notice'] )
	echo '<div id="message" class="updated"><p><strong>' . $_POST['notice'] . '</strong></p></div>';
	
if( $_POST['errornotice'] )
	echo '<div id="message" class="error"><p><strong>' . $_POST['errornotice'] . '</strong></p></div>';
?>
<form action="" method="post">
  <table class="form-table">
    <tbody>
      <tr>
        <td  id="titlediv"><input type="text" class="regular-text" value="<?php echo ($results[0]->checklist != '') ? stripslashes($results[0]->checklist):'';?>" id="title" name="checklist" placeholder="Name " /></td>
		<td><p style="float:right; padding-right:10px;" class="field switch"><span> Show Color Legend </span>
            <?php
	if(trim($_GET['flag']) == 'add')
	{
		echo '<input type="radio" id="radio1" name="status" value="1"  checked />
		<input type="radio" id="radio2" name="status" value="0" />
		<label for="radio1" class="cb-enable selected"><span>Enable</span></label>
		<label for="radio2" class="cb-disable"><span>Disable</span></label>';
		$acctxt='add';
		$btntxt='Add';
	}
	else
	{
		if($results[0]->status == 1)
		{
			echo '<input type="radio" id="radio1" name="status" value="1"  checked />
		<input type="radio" id="radio2" name="status" value="0" />
		<label for="radio1" class="cb-enable selected"><span>Enable</span></label>
		<label for="radio2" class="cb-disable"><span>Disable</span></label>';
		}
		else
		{
			echo '<input type="radio" id="radio1" name="status" value="1"   />
		<input type="radio" id="radio2" name="status" checked value="0" />
		<label for="radio1" class="cb-enable "><span>Enable</span></label>
		<label for="radio2" class="cb-disable selected"><span>Disable</span></label>';
		}
		$acctxt='edit';
		$btntxt='Update';
		
		echo '<input type="hidden"  name="cat_id" value="'.$results[0]->id.'"   />';
		}
?>
          </p></td>
      </tr>
      <tr>
        <td colspan="2"><div id="poststuff">
            <?php 
			if($results[0]->description != '')
			{
				the_editor($results[0]->description,'description');
			}
			else
			{
				the_editor('','description');
			}
			 ?>
          </div></td>
      </tr>
       
    </tbody>
  </table>
  <p class="submit">
    <input type="submit" value="<?php echo $btntxt;?>" class="btn btn-primary  active" id="submit" name="submit">
    <input name="action" value="<?php echo $acctxt;?>_checklist" type="hidden" />
  </p>
</form>



<?php if($results[0]->id != ''){ ?>
 <p class="submit addnewoption" style="text-align:right; padding-right:20px;">
    <a class="btn btn-primary" href="<?php echo get_option('siteurl').'/wp-admin/admin.php?page=checkery&flag=addoption&id='.$results[0]->id;?>">Add New Option</a>
	&nbsp;<a class="btn btn-primary" href="<?php echo get_option('siteurl').'/wp-admin/admin.php?page=checkery&flag=categoryadd&id='.$results[0]->id;?>">Add New Category</a>
  </p>
<table  id="table" cellspacing="0" width="90%">
<thead>
<tr>
<th>Option name</th>
<th  data-hide="phone,tablet">Category</th>
<th data-hide="phone,tablet">Status</th>
<th data-hide="phone,tablet">Date Created</th>
<th data-hide="phone,tablet">Action</th>
</tr>
</thead>


<?php 

$category_items=$wpdb->get_results("SELECT a.*,b.category FROM ".$wpdb->prefix."checkery_items a,".$wpdb->prefix."checkery_category b WHERE a.category=b.id AND a.checklist_id='".$_GET['id']."'"); 
$accurl = get_option('siteurl').'/wp-admin/admin.php?page=checkery';
foreach($category_items as $items){ ?>
<tbody>
<td><a href="<?php echo $accurl.'&flag=edit&itemid='.$items->id.'&id='.$items->checklist_id; ?>"><?php echo $items->check_item; ?></td>
<td><?php echo $items->category; ?></td>
<td>
<p class="field switch">
		<?php if($items->status == 1)
		{ 
			echo '<label for="radio1" class="cb-enable selected"><span>Enable</span></label>
		<label for="radio2" class="cb-disable"><span>Disable</span></label>';
		}
		else
		{
			echo '<label for="radio1" class="cb-enable "><span>Enable</span></label>
		<label for="radio2" class="cb-disable selected"><span>Disable</span></label>';
		}
		?>
</p></td>
<td><?php echo $items->date_created; ?></td>
<td>
<?php 

echo ' <a href="'.$accurl.'&flag=editoption&itemid='.$items->id.'&id='.$items->checklist_id.'" title="Edit Checklist Category
"><img src="'.plugins_url("images/edit.png", __FILE__).'" /></a>&nbsp;
		 <a onclick="return condelete();" href="'.$accurl.'&flag=deleteoption&itemid='.$items->id.'&id='.$items->checklist_id.'" title="Delete"><img src="'.plugins_url("images/delete.png", __FILE__).'" /></a>'; ?>
</td>
</tbody>
<?php } ?>

<tfoot>
<tr>
<td colspan="9"><div class="pagination pagination-centered"></div>
</td>
</tr>
</tfoot>
</table>

<?php } ?>

<script type="text/javascript">
$= jQuery.noConflict(); 
$(document).ready( function(){
	 $(function () {
        $('#table').footable();
		
	

    });
	$(".cb-enable").click(function(){
		var parent = $(this).parents('.switch');
		$('.cb-disable',parent).removeClass('selected');
		$(this).addClass('selected');
		$('.checkbox',parent).attr('checked', true);
	});
	$(".cb-disable").click(function(){
		var parent = $(this).parents('.switch');
		$('.cb-enable',parent).removeClass('selected');
		$(this).addClass('selected');
		$('.checkbox',parent).attr('checked', false);
	});
});

function condelete()
{
	return confirm('Are you sure if you want to delete this item');
}
</script>
