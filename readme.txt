=== Checkery ===
Contributors: SocialFuse, EnvisionYourWebsite
Donate link: http://donate.socialfuse.com
Tags: checklist, automatically assign checklist tracking
Requires at least: 3.0.1
Tested up to: 4.0
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Checkery allows you to track your customer checklist on any device. 

== Description ==

Checkery provides short codes to embed anywhere in your Wordpress website. We make it easy to use Checkery with Infusionsoft. You can track, create new checklist, and embed into your website simple and easy.

== Installation ==

This section describes how to install the plugin and get it set up and working.

1. Login to your Wordpress website
2. Navigate to the Plugins through the category in left column below the Dashboard. http://www.your-domain.com/wp-admin/plugins.php
3. Select "Add New" and in the field type Checkery by EnvisionYourWebsite.com
4. Download Checkery and Install the plugin
5. Activate Plugin


Alternative FTP installation method

1. Upload `Checkery.php` to the `/wp-content/plugins/` directory folder
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Place this string of code  `<?php do_action('plugin_name_hook'); ?>` in your templates

== Frequently Asked Questions ==

= Does Checkery work with Infusionsoft? =

Yes, Checkery is engineered to work with Infusionsoft.

= Will Checkery work with Salesforce and/or Exact Target? =

Yes, Checkery is build to work into you pages using Salesforce and/or Exact Target

= Will Checkery work with MailChimp or other email platforms? =

Yes, Checkery with will with any email response platform.

== Screenshots ==

1. This screen shot 1 with description 
2. This screen shot 2 with description 
3. This screen shot 3 with description 
4. This screen shot 4 with description 

== Changelog ==

= 1.0 =
* A change since the previous version.
* Another change.

= 0.5 =
* List versions from most recent at top to oldest at bottom.

== Upgrade Notice ==

= 1.0 =
Upgrade notices describe the reason a user should upgrade.  No more than 300 characters.

= 0.5 =
This version fixes a security related bug.  Upgrade immediately.

== Arbitrary section ==

Checkery is in active development and version control, revisions, updates and enhancements are being developed to better serve you and provide enhanced value.
